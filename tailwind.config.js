module.exports = {
  mode: 'jit',
  purge: [
    // ...
  ],
  theme: {
    boxShadow: {
      custom: '0px 5px 50px rgba(0, 0, 0, 0.15);'
    }
  }
  // ...
}
